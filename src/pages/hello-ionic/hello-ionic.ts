import { Component } from '@angular/core';
import { FileTransfer,FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';


@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})


export class HelloIonicPage {
  private url= "https://udv-researchdevelopment.carex.dk/codepush/main.css";

  constructor(private fileTransfer: FileTransfer, private file:File) {

  }
  getUpdates(e){
    e.preventDefault();
    const fileTransfer: FileTransferObject = this.fileTransfer.create();
    console.log(this.file);
    //let path = this.filePath.resolveNativePath(this.file.applicationDirectory + "www/" + assetsFilePath)
        fileTransfer.download(this.url, this.file.dataDirectory +'www/build/' + 'main2.css').then((entry) => {
          console.log('download complete: ' + entry.toURL());
          console.log('download complete: ' + entry);
        }, (error) => {
          // handle error
          console.log(error);
        });
  
  }
} 
